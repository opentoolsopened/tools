# Open Tools for Open Ed

A collaborative wiki of free, open software for ethical pedagogy. 

*Developed by Erin Glass (UCSD) and Nathan Schneider (CU Boulder)*

## Tools

*These need to be organized and categorized in some way*

### Discussion  
* [MLA Humanities Commons](https://hcommons.org/): Non-profit digital commons that allows users to build public or private Wordpress websites for course discussion, personal academic websites, and public academic resources. 
* [Commons in a Box](https://commonsinabox.org/): Host your own WordPress digital commons for your institution or group. 
* [Discourse](https://www.discourse.org/): Can be used a mailing list, discussion forum, long-form chat room. Install/host on your own for free or pay for the hosted version. 
* [Loomio](https://www.loomio.org/about): Discussion tool with a focus on decision making and roots in Occupy Wall Street. 
* [Rocket Chat](https://rocket.chat/): Slack alternative chat forum. 
* [Mastodon](https://joinmastodon.org/): Networked discussion tool similar to Twitter. 

### Annotation 
* [Hypothesis](tools/hypothesis.md): Annotate any webpage/online text for social reading. 
* [CommentPress](http://futureofthebook.org/commentpress/): WordPress theme that enables paragraph level comments of long form texts. Available on the MLA Humanities Commons or install on your own WordPress site. 

### Digital publishing/archive  
* [Scalar](https://scalar.me/anvc/): Platform for creating multimedia, non-linear academic and student texts. Developed at the University of Southern California. 
* [Omeka](https://omeka.org/):Platform for sharing digital collections and creating media-rich online exhibits. Developed at George Mason University. 
* [Neatline](http://neatline.org/): Platform for allowing students and scholars to tell stories with maps and timelines. Developed for Omeka.
* [Manifold](https://manifoldapp.org/docs/index):Academic-developed publishing platform great for iterative, media rich and/or long form academic texts. Allows for crowd-sourced text annotation. 
* 
### Word processing/text editing
* [LibreOffice](https://www.libreoffice.org/): FLOSS alternative to MS Office Suite. Includes software for word processing, spreadsheets, presentations, and more. 
* [Etherpad](http://etherpad.org/): Realtime, collaborative online text editor. 
* [CodiMD](https://demo.codimd.org/): Realtime, collaborative online text editor in markdown. FLOSS version of HackMD. 
* [Atom](https://atom.io/)

### Coding 
* [Jupyter Notebook](https://jupyter.org/): Web application for creating and sharing documents with interactive code. 
* [Atom](https://atom.io/)

### Search Engine
* [DuckDuckDo](https://duckduckgo.com/): Search engine that doesn't track user data. Note that it is not completely open source.
* [Searx](https://en.wikipedia.org/wiki/Searx): Self-hostable, free-software meta-search engine

### Citation Manager 
* Zotero

### Self hosting
* [Nextcloud](tools/nextcloud.md)

### Not categorized yet ... 
* Open Science Foundation
* [Sandstorm](tools/sandstorm.md)
* ScholarlyHub
* Wallabag
* FBReader
* Feeder
* [Pandoc](https://pandoc.org/): Convert files from one format to another. 
* [Voyant](https://voyant-tools.org/): Browser based tool for text mining. Great for beginners. 
* Cloudron
* [Collabora](tools/collabora.md)
* Tor

## Open questions

* What should we call this?
    - RMS suggests "Libre Tools for Libre Ed"
* What should be included on each entry?
    - Definition, features
    - Tags
    - Advantages and concerns
    - Links to example use cases
    - Links to relevant tutorials
* Just education or also research?
    - We're thinking education focus, with research as part of that
* How can we organize educators to help strengthen these platforms?
* What organizations might be interested in supporting this work?
* What forms of publication would advance our goals?
* How do we define our list of standards and goals?
* What tool should we use to organize this wiki?
    - MediaWiki
        - See pretty example: [Guerrilla Translation](https://wiki.guerrillamediacollective.org/index.php/Main_Page)
    - GitLab
    - Cloudron: Gogs git hosting + self-hosted GitHub Pages
    - WordPress with collaborative wiki/knowledgebase plugin
    - [FLOing Knowledge Template](https://airtable.com/universe/explC49GEtfjFCIhP/floing-knowledge-template) built in Airtable
    - Integrate Zotero for bibliographies?
* What projects might emerge from this documentation effort?
    - A package mixing VPS/Cloudron/etc. to make it easier for educators to deploy these tools
    - A campaign encouraging campuses to better support open as opposed to proprietary tools

## Open resources

* [alternativeTo - Open Source](https://alternativeto.net/platform/all/?license=opensource)
* [DIRT (Digital Research Tools) Directory](https://dirtdirectory.org/)
* Internet of Ownership - [Platform Co-op Stack](https://ioo.coop/directory/stack/) and [Cooperative Clouds](https://ioo.coop/clouds/)
* [Nathan's Open Work page](https://nathanschneider.info/open-work/)
* [No More Google](https://nomoregoogle.com/)
* [PRISM-Break](https://prism-break.org/)
* [Programming Historian](https://programminghistorian.org/) ([source](https://github.com/programminghistorian/jekyll))
* [Sarapis](https://sarapis.org/)

## Bibliography

Glass, Erin. "[Networks for Us, by Us](http://www.erinroseglass.com/designlarge-talk-networks-us-us/)." Design@Large lecture. University of California, San Diego. February 20, 2018.

Schneider, Nathan. "[The Joy of Slow Computing](https://newrepublic.com/article/121832/pleasure-do-it-yourself-slow-computing)." _New Republic_. May 19, 2015.

Schneider, Nathan. "[Why Researchers Shouldn’t Share All Their Data](https://nathanschneider.info/articles/UsingEverything.html). _Chronicle of Higher Education_. April 8, 2018.