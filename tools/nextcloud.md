# Nextcloud

Nextcloud is a self-hostable, open-source, cloud platform. By default it includes tools like file sharing (with accompanying mobile apps), calendars, and contacts that can replace commercial services like Google. It also has an [App Store](https://apps.nextcloud.com/) featuring a wide range of collaboration and productivity tools.

## Use cases

### Collaborative document editing

Nextcloud can integrate with the collaborative editing apps [Collabora](collabora.md) and OnlyOffice.

### Group file storage

## How to use it

Nextcloud can be used through third-party hosting services or deployed on one's own server. One easy way to deploy it is using the [Cloudron](cloudron.md) platform.

## Bibliography

## Related

* [Cloudron](cloudron.md)
* [Collabora](collabora.md)
* [Sandstorm](sandstorm.md)