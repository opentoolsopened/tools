# Hypothesis

Hypothesis is an open source, non-profit project designed to develop an open protocol for annotation across the web through the W3C. It is also specifically focused on supporting academic use.

Although the platform is centrally hosted and managed, the protocol-oriented approach of the organization enables the model to be developed in a more decentralized fashion.

## Use cases

### Encourage and evaluate active reading

* Nathan Schneider (CU Boulder)'s [Disruptive Entrepreneurship](https://nathanschneider.info/wiki/school/disruptive_entrepreneurship#online_participation) course

## How to use it

* [Frequently Asked Questions](https://web.hypothes.is/faq/)
* [Quick Start Guide for Students](https://web.hypothes.is/quick-start-guide-for-students/)
* [Teacher Resource Guide](https://web.hypothes.is/teacher-resource-guide/)

## Bibliography

## Related